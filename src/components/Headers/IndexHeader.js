import React from "react";

import { Container } from "reactstrap";

function IndexHeader() {
  return (
    <>
      <div
        className="page-header section-dark"
        style={{
          backgroundImage:
            "url(" + require("assets/img/afks.jpg").default + ")",
        }}
      >
        <div className="filter" />
        <div className="content-center">
          <Container>
            <div className="title-brand">
              <h1 className="presentation-title">Arctic-Play</h1>
            </div>
            <h2 className="presentation-subtitle text-center">
            АНО Арктическая федерация киберспорта
            </h2>
          </Container>
        </div>
        <h6 className="category category-absolute">
        Первая в Арктике киберспортивная федерация.
          
        </h6>
      </div>
    </>
  );
}

export default IndexHeader;

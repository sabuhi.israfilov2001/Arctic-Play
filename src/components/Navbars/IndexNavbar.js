import React from "react";
import classnames from "classnames";
import {
  Button,
  Collapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { Link } from "react-router-dom";


function IndexNavbar() {
  const [navbarColor, setNavbarColor] = React.useState("navbar-transparent");
  const [navbarCollapse, setNavbarCollapse] = React.useState(false);

  const toggleNavbarCollapse = () => {
    setNavbarCollapse(!navbarCollapse);
    document.documentElement.classList.toggle("nav-open");
  };

  React.useEffect(() => {
    const updateNavbarColor = () => {
      if (
        document.documentElement.scrollTop > 299 ||
        document.body.scrollTop > 299
      ) {
        setNavbarColor("");
      } else if (
        document.documentElement.scrollTop < 300 ||
        document.body.scrollTop < 300
      ) {
        setNavbarColor("navbar-transparent");
      }
    };

    window.addEventListener("scroll", updateNavbarColor);

    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor);
    };
  });
  return (
    <Navbar className={classnames("fixed-top", navbarColor)} expand="lg">
      <Container>
        <div className="navbar-translate">
          <NavbarBrand
            data-placement="bottom"
            href="/index"
            target="_blank"
            title="Arctic-Play"
          >
            Arctic-Play
          </NavbarBrand>
        </div>
        <Collapse
          className="justify-content-end"
          navbar
          isOpen={navbarCollapse}
        >
          <Nav navbar>

          <NavItem>
                <NavLink
                  href="/"
                >
                  <p>Главная</p>
                </NavLink>
          </NavItem>

          <NavItem>
                <NavLink
                  href="/"
                >
                  <p>Турниры</p>
                </NavLink>
          </NavItem>

          <NavItem>
                <NavLink
                  href="/"
                >
                  <p>Новости</p>
                </NavLink>
          </NavItem>

          <UncontrolledDropdown nav>
                <DropdownToggle
                  caret
                  color="default"
                  href="#csgo"
                  nav
                  onClick={(e) => e.preventDefault()}
                >
                  <p>Игры</p>
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem to="/index" tag={Link}>
                    CS GO
                  </DropdownItem>
                  <DropdownItem
                    target="_blank"
                  >
                    Dota 2
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>

          <NavItem>
              <Button
                className="btn-round"
                color="danger"
                href="/"
                target="_blank"
              >
                <i className="nc-icon nc-spaceship"></i> Регистрация
              </Button>
            </NavItem>

            <NavItem>
              <NavLink
                data-placement="bottom"
                href="https://vk.com/anoafk"
                target="_blank"
                title="Follow us on VK"
              >
                <i className="fa fa-vk" />
                <p className="d-lg-none">VK</p>
              </NavLink>
            </NavItem>

            <NavItem>
              <NavLink
                data-placement="bottom"
                href="https://Instagram.com/anoafk"
                target="_blank"
                title="Follow us on Instagram"
              >
                <i className="fa fa-instagram" />
                <p className="d-lg-none">Instagram</p>
              </NavLink>
            </NavItem>

            <NavItem>
              <NavLink
                data-placement="bottom"
                href="https://t.me/anoafk"
                target="_blank"
                title="Follow us on Telegram"
              >
                <i className="fa fa-telegram" />
                <p className="d-lg-none">Telegram</p>
              </NavLink>
            </NavItem>

            <NavItem>
            <NavLink
                data-placement="bottom"
                href="https://discord.gg/YQsXtAbf8s"
                target="_blank"
                title="Follow us on Discord"
              >
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-discord" viewBox="0 0 16 16">
  <path d="M6.552 6.712c-.456 0-.816.4-.816.888s.368.888.816.888c.456 0 .816-.4.816-.888.008-.488-.36-.888-.816-.888zm2.92 0c-.456 0-.816.4-.816.888s.368.888.816.888c.456 0 .816-.4.816-.888s-.36-.888-.816-.888z"/>
  <path d="M13.36 0H2.64C1.736 0 1 .736 1 1.648v10.816c0 .912.736 1.648 1.64 1.648h9.072l-.424-1.48 1.024.952.968.896L15 16V1.648C15 .736 14.264 0 13.36 0zm-3.088 10.448s-.288-.344-.528-.648c1.048-.296 1.448-.952 1.448-.952-.328.216-.64.368-.92.472-.4.168-.784.28-1.16.344a5.604 5.604 0 0 1-2.072-.008 6.716 6.716 0 0 1-1.176-.344 4.688 4.688 0 0 1-.584-.272c-.024-.016-.048-.024-.072-.04-.016-.008-.024-.016-.032-.024-.144-.08-.224-.136-.224-.136s.384.64 1.4.944c-.24.304-.536.664-.536.664-1.768-.056-2.44-1.216-2.44-1.216 0-2.576 1.152-4.664 1.152-4.664 1.152-.864 2.248-.84 2.248-.84l.08.096c-1.44.416-2.104 1.048-2.104 1.048s.176-.096.472-.232c.856-.376 1.536-.48 1.816-.504.048-.008.088-.016.136-.016a6.521 6.521 0 0 1 4.024.752s-.632-.6-1.992-1.016l.112-.128s1.096-.024 2.248.84c0 0 1.152 2.088 1.152 4.664 0 0-.68 1.16-2.448 1.216z"/>
                </svg>
                <i className="bi bi-discord" />
                <p className="d-lg-none">Discord</p>
              </NavLink>
            </NavItem>

            <NavItem>
              <NavLink
                data-placement="bottom"
                href="https://www.youtube.com/channel/UC_Gmg4RUfAyDIAUdkXlFHZQ/featured"
                target="_blank"
                title="Follow us on Youtube"
              >
                <i className="fa fa-youtube" />
                <p className="d-lg-none">Youtube</p>
              </NavLink>
            </NavItem>

          
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
}

export default IndexNavbar;
